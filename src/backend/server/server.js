const express = require("express");
const cors = require("cors");

// setup express & cors
const app = express();
app.use(express.json());
app.use(cors());

// custom routes
const db = require("./routes/api/db");
app.use("/api/db", db);

// static routes
app.use(express.static(__dirname + "/build/public/")); // set static folder
app.get(/.*/, (req, res) =>
    res.sendFile(__dirname + "/build/public/index.html")
);

module.exports = app;
