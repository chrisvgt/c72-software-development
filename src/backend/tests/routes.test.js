const request = require("supertest");
const app = require("../server/server");
const db = require("../server/WKADB");

afterAll((done) => {
    db.close();
    done();
});

describe("REST API", () => {
    test("POST /api/db/set", async () => {
        const res = await request(app).post("/api/db/set").send({
            example: "text",
        });
        expect(res.statusCode).toEqual(403);
    });
    test("GET /api/db/find", async () => {
        const res = await request(app).get("/api/db/find");
        expect(res.statusCode).toEqual(200);
    });
    test("GET /api/db/aggregate", async () => {
        const res = await request(app).get("/api/db/aggregate");
        expect(res.statusCode).toEqual(200);
    });
});
