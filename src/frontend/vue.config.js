const path = require("path");

module.exports = {
    outputDir: path.resolve("../backend/server/build/public"),
};
