const { MongoClient } = require("mongodb");
require("dotenv").config();

/**
 * ? db settings
 */
const uri = process.env.DB_URI;
const database = "wka";
const collection = "wka";

/**
 * ? create mongodb-client with uri
 *
 * because this is the only Client in use, connections do not need to be cleaned up, this is handled by the mongodb-client
 */
const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

/**
 * ? connect to the mongodb database
 */
async function connect() {
    try {
        // verify connection
        if (!client.isConnected()) {
            await client.connect();
            await client.db("admin").command({ ping: 1 });
        }
        return client.isConnected();
    } catch (error) {
        console.error("MongoDB-Connection failed : ", error.message);
    }
}

/**
 * ? close mongodb connection
 */
async function close() {
    try {
        if (client.isConnected()) {
            await client.close();
        }
    } catch (error) {
        console.error("MongoDB-Close failed : ", error.message);
    }
}

/**
 * ? find all documents in a collection that match provided filter and options
 *
 * @param {string} filter
 * @param {string} options
 * @returns object
 */
async function find(filter, options) {
    if (filter != null) {
        try {
            const coll = client.db(database).collection(collection);
            const result = await coll.find(filter, options).toArray();
            return result;
        } catch (error) {
            console.error("FIND ERROR: ", error);
        }
    }
    return null;
}

/**
 * ? execute a aggregation pipeline
 * ! you need to establish a connection before calling this function and close the connection afterwards
 * ! when the pipe is null (required filters not chosen [see WKAPipe.js]), returns a string
 * ! wkaData (in WKAFilter) throws an error, expects an object
 *
 * @param {string} pipeline
 * @param {string} options
 * @returns object or string
 */
async function aggregate(pipeline, options) {
    if (pipeline != null) {
        try {
            const coll = client.db(database).collection(collection);
            const result = await coll.aggregate(pipeline, options).toArray();
            return result;
        } catch (err) {
            console.error("AGGREGATE ERROR: ", err, pipeline);
        }
    }
    return null;
}

module.exports = {
    connect,
    close,
    find,
    aggregate,
};
