// Chart 1: Leistung der WKAs kumulativ über einen Zeitraum.
let chart1 = {
    type: "line",
    data: {
        labels: [],
        datasets: [
            {
                label: "",
                data: [],
                fill: true,
                borderColor: "red",
                spanGaps: true,
                showLine: true,
                pointRadius: 0,
            },
        ],
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            y: {
                title: {
                    display: true,
                    text: "Leistung in MW",
                    font: {
                        size: 16,
                    },
                },
            },
            x: {
                type: "time",
            },
        },
        plugins: {
            legend: {
                labels: {
                    font: {
                        size: 20,
                    },
                },
            },
        },
        elements: {
            point: {
                hitRadius: 20, //tooltip shows up, when you get to 20px near it, makes it easier to hit
            },
        },
    },
};

// Chart 2: Leistung der WKAs kumulativ pro Postleitzahl.
let chart2 = {
    type: "bar",
    data: {
        // random data for testing
        labels: [],
        datasets: [
            {
                label: "",
                data: [],
                backgroundColor: "#333",
            },
        ],
    },
    options: {
        animation: false,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            y: {
                title: {
                    display: true,
                    text: "Leistung in MW",
                    font: {
                        size: 16,
                    },
                },
            },
        },
        plugins: {
            legend: {
                labels: {
                    font: {
                        size: 20,
                    },
                },
            },
        },
    },
};

// Chart 1: Leistung der WKAs kumulativ über einen Zeitraum.
// Chart 2: Leistung der WKAs kumulativ pro Postleitzahl.
module.exports = {
    chart1,
    chart2,
};
