const express = require("express");
const router = express.Router();
const db = require("../../WKADB");
const pip = require("../../WKAPipe");

// wkaFilter variables
let tmp;
let filter;
let options;
let pipeline = [];

//////////////////////
//       setter     //
//////////////////////
// set all wkaFilter variables for further calculations
router.post("/set", async (req, res) => {
    try {
        // receive and set db search parameter
        filter = JSON.parse(JSON.stringify(req.body)).filter;
        options = JSON.parse(JSON.stringify(req.body)).options;
        tmp = JSON.parse(JSON.stringify(req.body)).pipeline;

        // generate pipelines with WKAPipe
        pipeline = pip.genPipe(tmp);

        res.sendStatus(200);
    } catch (error) {
        res.sendStatus(403);
    }
});

///////////////////
//     find      //
///////////////////
// mongodb find method
router.get("/find", async (_req, res) => {
    try {
        await db.connect();
        db.find(filter, options).then(function (result) {
            res.status(200).send(result);
        });
    } catch (error) {
        res.sendStatus(403);
    }
});

///////////////////
//   aggregate   //
///////////////////
// mongodb aggregate method
router.get("/aggregate", async (_req, res) => {
    try {
        let arr = [];
        let tmp = [];
        await db.connect();
        pipeline.forEach((element, index) => {
            tmp.push(
                db.aggregate(element, options).then(function (result) {
                    arr[index] = result;
                })
            );
        });
        await Promise.all(tmp);
        res.status(200).send(arr);
    } catch (error) {
        res.sendStatus(403);
    }
});

module.exports = router;
