# MeVueNoGood

Repository for the project task in the software development module.

Content of the project is the graphical analysis of wind turbine data for a selectable time period.

- number of wind turbines
- zoomable display in a GeoMappingTool
- statistics

### Webstack

We use the MEVN stack for the implementation of the project.
That's also how our project name was created :)

    M – MongoDB
    E – ExpressJS
    V – VueJS
    N – NodeJS

### Folder structure

    └── src
        ├── backend
        └── frontend

The backend folder contains our web framework and the driver for MongoDB to connect to a database.

The frontend folder contains our Vue3 application.

## Getting Started

### Requirements

The following must be installed to use the project.

- nodejs, npm
- [@vue/cli](https://cli.vuejs.org/)
- [nodemon](https://www.npmjs.com/package/nodemon) (optional for development)

### Setup project

Go to the directory `/src/backend` and install the node_modules with `npm install`.
The same must also be done with the frontend.
When packages are missing run `npm install`.

### How to develop

#### Backend

    npm run start
    npm run dev           (for development)

#### Frontend

    npm run serve         (for development)
    npm run build:release (for production)

## Explore MongoDB

### Requirements

[MongoDB Compass](https://github.com/mongodb-js/compass)

### How to connect

Connection string:

`mongodb://<username>:<password>@<host>:<port>/?authSource=<authentication-database>`
