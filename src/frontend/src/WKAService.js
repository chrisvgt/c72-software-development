import axios from "axios";
const url = process.env.VUE_APP_BACKEND_URL || "http://localhost:5000/api";
class WKAService {
    // post request a object to backend api
    static postData(path, obj) {
        return new Promise((resolve, reject) => {
            axios
                .post(url + path, obj)
                .then((res) => {
                    if (res.status === 200) {
                        resolve();
                    } else {
                        // POST FAILED
                        throw new Error(
                            `POST failed with status ${res.status}: ${res.statusText}, expected 200`
                        );
                    }
                })
                .catch((err) => {
                    // POST FAILED WITH ERROR
                    reject(err);
                });
        });
    }
    // get request object from backend api
    static getData(path) {
        //gets all data from all WKAs asynchronously
        return new Promise((resolve, reject) => {
            axios
                .get(url + path)
                .then((res) => {
                    //axios returns a promise that we want to resolve, GET SUCCESS
                    const data = res.data;
                    if (data) {
                        // if no data found but executed successfully throw error
                        if (data.length == 0) {
                            throw "Keine Daten gefunden.";
                        }
                        //forward data if data is returned
                        resolve(data);
                    } else {
                        //nothing if no data is returned
                    }
                })
                .catch((err) => {
                    //GET ERROR
                    reject(err);
                });
        });
    }
}

export default WKAService;
