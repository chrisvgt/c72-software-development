//the following functions can be used to dynamically generate pieces of the pipeline
//the pipeline sanitizes data from the database.
//is does not handle situations (e.g. certain values being "null") that do not occur in the database at the time of development.

//generates an aggregation expression that can be used in a match stage to match if the date a given field contains, is between two given dates
//start and end date are both inclusive
function genMatchDateToTimePeriod(fieldName, startDate, endDate) {
    let r = {};
    r[fieldName] = {
        $gte: new Date(startDate),
        $lte: new Date(endDate),
    };
    return r;
}

//generates an aggregation expression to merge and convert the data in the Inbetriebn- and Alt_an_anz-field to either a string or a date
//if Inbetriebn is not empty, use (and prefer) Inbetriebn (to Alt_an_anz) and then try to convert it to a date
//else if Alt_an_anz is not empty, use Alt_an_anz and try to convert it to a date
//if both are empty strings, this will set return null.
//if the value in the fields is not a valid date, this will generate an error 
//everything else will produce an Error
function genMergeInbetriebAlt_an_anzConvertToDate() {
    return {
        $dateFromString: { //convert from String to Date
            dateString: {
                $switch: {
                    branches: [
                        { //when Inbetriebn is not empty, use (and prefer) Inbetriebn (to Alt_an_anz)
                            case: {
                                $ne: ["$Inbetriebn", ""],
                            },
                            then: "$Inbetriebn",
                        },
                        { //else if Alt_an_anz is not empty, use Alt_an_anz
                            case: {
                                $ne: ["$Alt_an_anz", ""],
                            },
                            then: "$Alt_an_anz",
                        },
                    ],
                    default: null, //if both are empty, set null. this can later be changed into "siehe Status" in the dataModificationStage.
                },
            },
            onError: "Error converting Inbetriebnahme-String to Date",
        }
    };
}

//generates an aggregation expression to convert the data in the Genehmigt-field to either a string or a date
//if the field is empty, it will say null, if it does contain a string-formatted date, it will be converted to a date.
//everything else will produce an Error
function genConvertGenehmigtToDate() {
    return {
        $cond: {
            if: { //if Genehmigt is empty, set it to null, otherwise convert it from a string to a date
                $eq: ["$Genehmigt", ""],
            },
            then: null,
            else: {
                $dateFromString: {
                    dateString: "$Genehmigt",
                    onError: "Error converting Genehmigt-String to Date",
                },
            },
        },
    };
}

//generates an aggregation expression to convert a date in the field specified by fieldName to a string (in the german dd.MM.YYYY format)
//if the field does not contain a date, the data is just kept as it is
function genConvertDateToString(fieldName) {
    return { //if fieldName is not a date, just pass it though as is (e.g. null). otherwise convert the date back to a string (german format).
        $cond: {
            if: {
                $ne: [
                    {
                        $type: `$${fieldName}`,
                    },
                    "date",
                ],
            },
            then: `$${fieldName}`,
            else: {
                $dateToString: {
                    format: "%d.%m.%Y",
                    date: `$${fieldName}`,
                },
            },
        },
    };
}

//generates an aggregation expression to convert data in the field specified by fieldName to a double
//if the input data is not a string, just try to convert it
//if the data is a string, replace the first (and hopefully only) "," with a ".". This is how the function handels string-formatted floating point numbers that use the decimal comma.
function genConvertToDouble(fieldName) {
    return {
        $convert: { //convert field specified by fieldName to a double (from either int or string).
            input: {
                $cond: {
                    if: { //if fieldName is not a string, just convert it. if fieldName is a string, replace all "," with ".", because the string-formatted floating point numbers in the database are separated with a comma, while mongodb expects a dot.
                        $ne: [
                            {
                                $type: `$${fieldName}`,
                            },
                            "string",
                        ],
                    },
                    then: `$${fieldName}`,
                    else: { //replace ","" with "."
                        $replaceOne: {
                            input: `$${fieldName}`,
                            find: ",",
                            replacement: "."
                        }
                    },
                }
            },
            to: "double",
            onError: `Error converting ${fieldName} to double`
        }
    };
}

//generates an aggregation expression to convert any data in the field specified by fieldName to a string
function genConvertToString(fieldName) {
    return { // convert field specified by fieldName to a string
        $convert: {
            input: `$${fieldName}`,
            to: "string",
            onError: `Error converting ${fieldName} to string`
        }
    };
}

//this function will generate an aggregation expression to replace a fields content with the supplied object/data "message", if the fields value is null
//message is directly inserted into the mongodb, so make sure, it follows proper mongodb syntax including (if applicable) the outer braces/brackets
function genSetIfNull(fieldName, message) {
    return {
        $ifNull: [
            `$${fieldName}`,
            message
        ]
    };
}

//this function will generate an aggregation expression to replace a fields content with the supplied object/data "message", if the fields value is equal to the specified x
//if the values are not equal, the value will remain unchanged
//message and x are directly inserted into the mongodb, so make sure, it follows proper mongodb syntax including (if applicable) the outer braces/brackets
//this function creates 
function genSetIfX(fieldName, x, message) {
    return {
        $cond: {
            if: {
                $eq: [
                    `$${fieldName}`,
                    x
                ],
            },
            then: message,
            else: `$${fieldName}`
        }
    };
}

//this function generates an aggregation expression to add a field "earliestDate" to each object which contains the earliest date found in either Inbetriebnahme (which has to be generated first, see genMergeInbetriebAlt_an_anzConvertToDate()) or Genehmigt
//if both Inbetriebnahme and Genehmigt are not Dates, then null will be inserted.
function genFindEarliestDate() {
    return {
        '$let': {
            'vars': {
                'ti': {
                    '$type': '$Inbetriebnahme'
                },
                'tg': {
                    '$type': '$Genehmigt'
                }
            },
            'in': {
                '$switch': {
                    'branches': [
                        {
                            'case': {
                                '$and': [
                                    {
                                        '$eq': [
                                            '$$ti', 'date'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$$tg', 'date'
                                        ]
                                    }
                                ]
                            },
                            'then': {
                                '$min': [
                                    '$Inbetriebnahme', '$Genehmigt'
                                ]
                            }
                        }, {
                            'case': {
                                '$and': [
                                    {
                                        '$eq': [
                                            '$$ti', 'date'
                                        ]
                                    }, {
                                        '$ne': [
                                            '$$tg', 'date'
                                        ]
                                    }
                                ]
                            },
                            'then': '$Inbetriebnahme'
                        }, {
                            'case': {
                                '$and': [
                                    {
                                        '$ne': [
                                            '$$ti', 'date'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$$tg', 'date'
                                        ]
                                    }
                                ]
                            },
                            'then': '$Genehmigt'
                        }
                    ],
                    'default': null
                }
            }
        }
    };
}

//generates a round instruction in an aggregation expression (set stage) that rounds the specified fields value to the specified place (-1 => 10, 0 => 1, 1 => 0.1, 2 => 0.01) 
//fieldname = string, place = int
function genRoundField(fieldName, place){
    return {$round: [`$${fieldName}`, place ]};
}

//retuns a pipeline object that aggregates data from the database.
//the pipeline sums the power (Leistung) of all wkas that have an Inbetriebnahme field matching the filter input in every PLZ.
//it then returns the top 10 PLZs with the highest accumulated Leistung and the accumulated Leistung in a field called "totalP".
function genPLZPipeline(pipelineParameters) {
    const pipeline = [
        {//Stage 0, setupStage, combine Alt_an_Anz and Inbetriebn into Inbetriebnahme
            $set: {},
        },
        { //removes unnecessary data
            $unset: [
                'Inbetriebn',
                'Alt_an_anz',
                'Betreiber',
                'Bst_Nr',
                'Bst_Name',
                'Ort',
                'Ortsteil',
                'Anl_Nr',
                'Anl_Bez',
                'Genehmigt',
                'Nordwert',
                'Ostwert',
                'Latitude',
                'Longitude',
                'Kreis',
                'Status',
                'Geme_Kenn',
                'Nabenhoehe',
                'Rotordurch',
                'LW_TAG',
                'LW_Nacht',
                'Stand_Abw',
                'Wka_ID'
            ],
        },
        { //Stage 2, matchStage
            $match: {},
        },
        { //Stage 3, dataConversionStage
            $set: {},
        },
        { //groups by PLZ and accumulate Leistung into totalP
            '$group': {
                '_id': '$PLZ',
                'totalP': {
                    '$sum': '$Leistung'
                }
            }
        },
        { //Stage 5, dataModificationStage
            '$set': {},
        },
        { //sorts descending
            '$sort': {
                'totalP': -1
            }
        },
        { //limits to top 10
            '$limit': 10
        }
    ];

    const setupStageIndex = 0;
    const matchStageIndex = 2;
    const dataConversionStageIndex = 3;
    const dataModificationStageIndex = 5;

    pipeline[setupStageIndex]["$set"]["Inbetriebnahme"] = genMergeInbetriebAlt_an_anzConvertToDate();

    pipeline[matchStageIndex]["$match"] = genMatchDateToTimePeriod("Inbetriebnahme", pipelineParameters.startDate, pipelineParameters.endDate);

    pipeline[dataConversionStageIndex]["$set"]["PLZ"] = genConvertToString("PLZ");
    pipeline[dataConversionStageIndex]["$set"]["Leistung"] = genConvertToDouble("Leistung");

    pipeline[dataModificationStageIndex]["$set"]["totalP"] = genRoundField("totalP", 2);

    //Debug:
    //console.log("PLZ Pipeline:");
    //console.log(JSON.stringify(pipeline, null, 4));

    return pipeline;
}

function genTotalPowerPipeline(pipelineParameters){
    return [
        {
            '$set': { //merge Inbetriebn and AltAnAnz into InbetriebnahmeAsDate. This could be replaced by genMergeInbetriebAlt_an_anzConvertToDate().
                'InbetriebnahmeAsDate': {
                    '$dateFromString': {
                        'dateString': {
                            '$switch': {
                                'branches': [
                                    {
                                        'case': {
                                            '$ne': [
                                                '$Inbetriebn', ''
                                            ]
                                        }, 
                                        'then': '$Inbetriebn'
                                    }, {
                                        'case': {
                                            '$ne': [
                                                '$Alt_an_anz', ''
                                            ]
                                        }, 
                                        'then': '$Alt_an_anz'
                                    }
                                ], 
                                'default': null
                            }
                        }, 
                        'onError': 'Error converting Inbetriebnahme-String to Date', 
                        'onNull': null
                    }
                }
            }
        }, {
            '$unset': [ //removes unneeded fields
                'Betreiber', 'Bst_Nr', 'Bst_Name', 'Ort', 'Ortsteil', 'Anl_Nr', 'Anl_Bez', 'Genehmigt', 'Ostwert', 'Nordwert', 'Latitude', 'Longitude', 'Kreis', 'Geme_Kenn', 'PLZ', 'Status', 'Nabenhoehe', 'Rotordurch', 'LW_TAG', 'LW_Nacht', 'Stand_Abw', 'Wka_ID', 'Alt_an_anz', 'Inbetriebn'
            ]
        }, {
            '$match': { //removes all wkas that have not been turned on or that have errors in "Inbetriebnahme"
                'InbetriebnahmeAsDate': {
                    '$ne': null
                }
            }
        }, { //converts leistung to a double, this could be replaced by genConvertToDouble()
            '$set': {
                'Leistung': {
                    '$convert': {
                        'input': {
                            '$cond': {
                                'if': {
                                    '$ne': [
                                        {
                                            '$type': '$Leistung'
                                        }, 'string'
                                    ]
                                }, 
                                'then': '$Leistung', 
                                'else': {
                                    '$replaceOne': {
                                        'input': '$Leistung', 
                                        'find': ',', 
                                        'replacement': '.'
                                    }
                                }
                            }
                        }, 
                        'to': 'double', 
                        'onError': 'Error converting Leistung to double', 
                        'onNull': null
                    }
                }
            }
        }, { //groups all wkas where Inbetriebnahme contains the same date and sums up the Leistung of every group. the first day (startDate) is treated specially as it also contains (additionally to the total power of that day, if applicable) the total power of all WKAs where Inbetriebnahme < startDate
            '$group': {
                '_id': {
                    '$cond': {
                        'if': {
                            '$lt': [
                                '$InbetriebnahmeAsDate', new Date(pipelineParameters.startDate)
                            ]
                        }, 
                        'then': new Date(pipelineParameters.startDate), 
                        'else': '$InbetriebnahmeAsDate'
                    }
                }, 
                'dayTotal': {
                    '$sum': '$Leistung'
                }
            }
        }, { //sorts the data ascending
            '$sort': {
                '_id': 1
            }
        }, { //removes all data not in the specified period
            '$match': {
                '$and': [
                    {
                        '_id': {
                            '$gte': new Date(pipelineParameters.startDate), 
                            '$lte': new Date(pipelineParameters.endDate)
                        }
                    }
                ]
            }
        }, { //converts inbetriebnahme to a string, this can be replaced by genConvertDateToString()
            '$set': {
                'Inbetriebnahme': {
                    '$cond': {
                        'if': {
                            '$ne': [
                                {
                                    '$type': '$_id'
                                }, 'date'
                            ]
                        }, 
                        'then': '$_id', 
                        'else': {
                            '$dateToString': {
                                'format': '%d.%m.%Y', 
                                'date': '$_id'
                            }
                        }
                    }
                }
            }
        }
    ];
}

//generates a pipeline object from the received filters, that can aggregates all matching wkas and their data from the database to be displayed on the map
function genMapPipeline(pipelineParameters) {
    if (
        !pipelineParameters.startDate
        || !pipelineParameters.endDate
    ) {
        //filters are disabled, removing this check, sanitizes and matches everything
        return null;
    }

    const noData = "keine Daten"; //message to show, when there's no data
    const roundingPlace = 2; //round numbers to 0.01 (10^-2)

    const setupStageIndex = 0;
    const removeDataStageIndex = 1;
    const earliestDateStageIndex = 2;
    const matchStageIndex = 3; //index of the match stage in the pipeline array
    // const sortStageIndex = 4; //this stage is currently hardcoded
    const dataConversionStageIndex = 5;
    const roundingStageIndex = 6;
    const dataModificationStageIndex = 7;

    /* Pipeline Stages
     * Stages might be dynamically filled with aggregation expressions
     * 0. setupStage: merge Inbetriebn and Alt_an_anz into Inbetriebnahme, convert Inbetriebnahme and Genehmigt to Date for sorting
     * 1. removeDataStage: removes unused fields (Inbetriebn and Alt_an_anz)
     * 2. this stage adds a field "earliestDate" with the earliest date in Inbetriebnahme or Genehmigt (whichever is earlier). As this relies on valid data in the Inbetriebnahme and Genehmigt field, it needs to follow after the first stage is completet. If both fields are null, null is the result. 
     * 3. matchStage: conditions to match data against (apply filters)
     * 4
     * 5. dataConversionStage: sanitizing stage 1, changes data types and checks validity (sets to null if data is unknown)
     * 6. roundingStage: rounding 
     * 7. dataModificationStage: sanitizing stage 2, adds hints like "siehe Status" or "keine Daten" if a field is null
     * 
     * sortStage: currently, there is a hardcoded stage used to sort the data by ascending Inbetriebnahme-date
    */

    const pipeline = [
        {//Stage 0, setupStage
            $set: {},
        },
        { //Stage 1, removeDataStage
            $unset: [],
        },
        { //Stage 2, add earliestDate
            $set: {}
        },
        { //Stage 3, matchStage
            $match: {},
        },
        { //Stage 4, sortStage
            $sort: {
                Inbetriebnahme: 1, //sort ascending by Inbetriebnahme
                Genehmigt: 1 //use Genehmigt (ascending) as fallback
            }
        },
        { //Stage 5, dataConversionStage
            $set: {},
        },
        { // Stage 6, roundingStage
            $set: {},
        },
        { //Stage 7, dataModificationStage
            $set: {},
        }
    ];

    //perform every time for the pipeline
    pipeline[setupStageIndex]["$set"]["Inbetriebnahme"] = genMergeInbetriebAlt_an_anzConvertToDate();
    pipeline[setupStageIndex]["$set"]["Genehmigt"] = genConvertGenehmigtToDate();

    pipeline[removeDataStageIndex]["$unset"].push("Inbetriebn");
    pipeline[removeDataStageIndex]["$unset"].push("Alt_an_anz");

    pipeline[dataConversionStageIndex]["$set"]["Inbetriebnahme"] = genConvertDateToString("Inbetriebnahme");
    pipeline[dataConversionStageIndex]["$set"]["Genehmigt"] = genConvertDateToString("Genehmigt");
    pipeline[dataConversionStageIndex]["$set"]["PLZ"] = genConvertToString("PLZ");
    pipeline[dataConversionStageIndex]["$set"]["Anl_Nr"] = genConvertToString("Anl_Nr");
    pipeline[dataConversionStageIndex]["$set"]["Leistung"] = genConvertToDouble("Leistung");
    pipeline[dataConversionStageIndex]["$set"]["Nabenhoehe"] = genConvertToDouble("Nabenhoehe");
    pipeline[dataConversionStageIndex]["$set"]["Rotordurch"] = genConvertToDouble("Rotordurch");
    pipeline[dataConversionStageIndex]["$set"]["LW_TAG"] = genConvertToDouble("LW_TAG");
    pipeline[dataConversionStageIndex]["$set"]["LW_Nacht"] = genConvertToDouble("LW_Nacht");
    pipeline[dataConversionStageIndex]["$set"]["Wka_ID"] = genConvertToString("Wka_ID");

    pipeline[roundingStageIndex]["$set"]["Leistung"] = genRoundField("Leistung", roundingPlace);
    pipeline[roundingStageIndex]["$set"]["Nabenhoehe"] = genRoundField("Nabenhoehe", roundingPlace);
    pipeline[roundingStageIndex]["$set"]["Rotordurch"] = genRoundField("Rotordurch", roundingPlace);
    pipeline[roundingStageIndex]["$set"]["LW_TAG"] = genRoundField("LW_TAG", roundingPlace);
    pipeline[roundingStageIndex]["$set"]["LW_Nacht"] = genRoundField("LW_Nacht", roundingPlace);
    
    pipeline[dataModificationStageIndex]["$set"]["Inbetriebnahme"] = genSetIfNull("Inbetriebnahme", "keine Daten");
    pipeline[dataModificationStageIndex]["$set"]["Genehmigt"] = genSetIfNull("Genehmigt", "keine Daten");
    pipeline[dataModificationStageIndex]["$set"]["Nabenhoehe"] = genSetIfX("Nabenhoehe", 0, noData);
    pipeline[dataModificationStageIndex]["$set"]["Rotordurch"] = genSetIfX("Rotordurch", 0, noData);
    pipeline[dataModificationStageIndex]["$set"]["LW_TAG"] = genSetIfX("LW_TAG", -99, noData);
    pipeline[dataModificationStageIndex]["$set"]["LW_Nacht"] = genSetIfX("LW_Nacht", -99, noData);
    pipeline[dataModificationStageIndex]["$set"]["Ortsteil"] = genSetIfX("Ortsteil", "", noData);
    pipeline[dataModificationStageIndex]["$set"]["Stand_Abw"] = genSetIfX("Stand_Abw", -99, noData);


    //add content to the $match stage
    if (pipelineParameters.startDate && pipelineParameters.endDate) {
        //filters can only be applied, when the start and end date are set
        //$and may not be empty and thus get generated when an entry for the $and is generated
        "$and" in pipeline[matchStageIndex]["$match"] || (pipeline[matchStageIndex]["$match"]["$and"] = []); //create "$and" if is does not exist yet. https://stackoverflow.com/a/38034202 <3
        if (pipelineParameters.genehmigt === true) {
            pipeline[matchStageIndex]["$match"]["$and"].push(genMatchDateToTimePeriod("Genehmigt", pipelineParameters.startDate, pipelineParameters.endDate));
        }

        if (pipelineParameters.inbetrieb === true) {
            pipeline[matchStageIndex]["$match"]["$and"].push(genMatchDateToTimePeriod("Inbetriebnahme", pipelineParameters.startDate, pipelineParameters.endDate));
        }

        if (pipelineParameters.inbetrieb === false && pipelineParameters.genehmigt === false) {
            pipeline[earliestDateStageIndex]["$set"]["earliestDate"] = genFindEarliestDate(); //add the earliestDate field
            pipeline[matchStageIndex]["$match"]["$and"].push(genMatchDateToTimePeriod("earliestDate", pipelineParameters.startDate, pipelineParameters.endDate));
        }
    }

    //ensure, there are no empty stages in the pipeline
    for (let index = pipeline.length - 1; index >= 0; --index) { //backwards, so array modifications though splice() don't fuck up the indices

        for (let stage in pipeline[index]) { //check within an object in the pipeline array, if there are empty stages (there should at max be one). if so, remove it
            if (Object.keys(pipeline[index][stage]).length === 0 && pipeline[index][stage].constructor === Object) {
                delete pipeline[index][stage];
            }
        }

        //if the object in the pipeline array is empty after the removal, remove this object from the array entirely (otherwise mongodb will throw an error)
        if (Object.keys(pipeline[index]).length === 0 && pipeline[index].constructor === Object) {
            pipeline.splice(index, 1);
        }
    }

    //Debug:
    //console.log("Map Pipeline:");
    //console.log(JSON.stringify(pipeline, null, 4));

    return pipeline;
}

module.exports = {
    //returns an array of pipelines to be executed to receive different datasets
    genPipe: function (pipelineParameters) {
        let arr = [
            genMapPipeline(pipelineParameters), //pipeline for map
            genTotalPowerPipeline(pipelineParameters), //pipeline for statistic 1 (chart1)
            genPLZPipeline(pipelineParameters), //pipeline for statistic 3 (chart2)
        ];
        
        return arr;
    },
};
