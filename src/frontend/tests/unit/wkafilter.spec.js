import { mount } from "@vue/test-utils";
import WKAFilter from "@/components/WKAFilter.vue";

/**
 * factory
 * ? mount wkafilter and merge the values object into data
 */
const factory = (values = {}) => {
    return mount(WKAFilter, {
        data() {
            return {
                ...values,
            };
        },
    });
};

describe("WKAFilter", () => {
    test("filters exist check", async () => {
        const wrapper = factory();
        expect(wrapper.find("#startDate").exists()).toBe(true);
        expect(wrapper.find("#endDate").exists()).toBe(true);
        expect(wrapper.find("#genehmigt").exists()).toBe(true);
        expect(wrapper.find("#inbetrieb").exists()).toBe(true);
        expect(wrapper.find("#btnmap").exists()).toBe(true);
        expect(wrapper.find("#btnstatistic").exists()).toBe(true);
    });

    test("map, statistic changing", async () => {
        const wrapper = factory();

        // emulate click on statistic button
        await wrapper.find("#btnstatistic").trigger("click");
        expect(wrapper.componentVM.getShowMap).toBe(false);

        // emulate click on map button
        await wrapper.find("#btnmap").trigger("click");
        expect(wrapper.componentVM.getShowMap).toBe(true);
    });

    test("check date input", async () => {
        const wrapper = factory();
        const input1 = wrapper.find("#startDate");
        const input2 = wrapper.find("#endDate");
        await input1.setValue("1998-07-24");
        await input2.setValue("2021-06-02");

        /**
         * ! WARNING: on date change component starts load function in wkafilter --> results in promise error
         * TODO way to fix? should we fix?
         * ? unit test fail? --> only warning message
         */

        expect(wrapper.componentVM.getStartDate).toBe("1998-07-24");
        expect(wrapper.componentVM.getEndDate).toBe("2021-06-02");
    });

    test("count wkaData", async () => {
        const wrapper = factory({ count: 42 });
        expect(wrapper.componentVM.getCount).toBe(42);
    });
});
